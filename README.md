# lotz-env
---

## Command list
```
$ scl enable devtoolset-7 bash
$ make init
$ make patch
$ make build
$ make install
```

## Clone Lotz_development and download tsc 

```
$ make init
```

## Patch minor changes into 

```
$ make patch
```


## x86_64


* Set GCC7 on CentOS
```
lotz-env (master)$ scl enable devtoolset-7 bash
```

* Check what they are in

```
lotz-env (master)$ tree -L 1 Lodz_development/

Lodz_development/
├── [icssev     54]  configure
├── [icssev   4.0K]  doc
├── [icssev   4.0K]  fw
├── [icssev   4.0K]  lib
├── [icssev   4.0K]  modules
├── [icssev    307]  readme.md
└── [icssev   4.0K]  sw

5 directories, 2 files
```

* Remove all symbolic links within

```
lotz-env (master)$ find -type l -delete
```


* Build it

```
lotz-env (master)$ make build
```

* Install it
Executable files are located in bin path.

```
lotz-env (master)$ make install
lotz-env (master)$ tree bin/
bin/
├── [ceauser  275K]  cb_tester
├── [ceauser   71K]  dma_tester
├── [ceauser  279K]  mem_dma_reader_tsc
├── [ceauser   24K]  mem_perf_tester
└── [ceauser   19K]  reg_gwj
```


* Clean, and leave Executable only

```
lotz-env (master)$ make clean
```

* Remove all
```
lotz-env (master)$ make distclean

```
* Exit gcc 7
```
lotz-env (master)$ exit
exit
lotz-env (master)$ gcc -dumpversion
4.8.5
```

## IFC14XX QorIQ SDK SDK installer version 2.6-4.14

Note that HDF5 should be in toolchain. 

In order to compile them, we need the latest SDK for IFC14XX. Please see (PREREQUIRE.md)[PREREQUIRE.md]. 

```
$ lotz-env (master)$ source /opt/ifc14xx/2.6-4.14/environment-setup-ppc64e6500-fsl-linux
$ lotz-env (master)$ make clean
$ lotz-env (master)$ make build
$ lotz-env (master)$ make clean
$ lotz-env (master)$ tree -L 1 Lodz_development/
Lodz_development/
├── [icssev   3.8K]  blm.Makefile
├── [icssev   321K]  cb_tester
├── [icssev     54]  configure
├── [icssev   166K]  dma_tester
├── [icssev   4.0K]  doc
├── [icssev   4.0K]  fw
├── [icssev   4.0K]  lib
├── [icssev   322K]  mem_dma_reader_tsc
├── [icssev    93K]  mem_perf_tester
├── [icssev   4.0K]  modules
├── [icssev    307]  readme.md
├── [icssev    77K]  reg_gwj
└── [icssev   4.0K]  sw


```

```
lotz-env (master)$ uname -ar
Linux icslab-ser03 3.10.0-862.14.4.el7.x86_64 #1 SMP Wed Sep 26 15:12:11 UTC 2018 x86_64 x86_64 x86_64 GNU/Linux
lotz-env (master)$ readelf -h Lodz_development/mem_perf_tester 
ELF Header:
  Magic:   7f 45 4c 46 02 02 01 00 00 00 00 00 00 00 00 00 
  Class:                             ELF64
  Data:                              2's complement, big endian
  Version:                           1 (current)
  OS/ABI:                            UNIX - System V
  ABI Version:                       0
  Type:                              DYN (Shared object file)
  Machine:                           PowerPC64
  Version:                           0x1
  Entry point address:               0x1fbe8
  Start of program headers:          64 (bytes into file)
  Start of section headers:          92640 (bytes into file)
  Flags:                             0x1, abiv1
  Size of this header:               64 (bytes)
  Size of program headers:           56 (bytes)
  Number of program headers:         8
  Size of section headers:           64 (bytes)
  Number of section headers:         38
  Section header string table index: 37
  
```
