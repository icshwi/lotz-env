This is the *short-term* repository for HDF5, which is used for Lotz development.
==




This version is actually is *HDF5 version 1.8.19 released on 2017-06-15*. We didn't check this is the exact same version of the official HDF5 1.8.19.

The original and official release is https://gitlab.esss.lu.se/icshwi/hdf5 also.

Note that HDF5 doesn't support the cross compilation [1]. 

```
git clone https://github.com/jeonghanlee/hdf5-env
$ cd hdf5-env/
$ hdf5-env (master)$ make init
$ hdf5-env (master)$ make build
$ hdf5-env (master)$ make install
$ hdf5-env (master)$ make setup
```



```


# References

[1] https://portal.hdfgroup.org/pages/viewpage.action?pageId=48808266

