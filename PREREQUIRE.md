## Cross Compiler Setup

```
[root@icslab-ser03 icssev]# ./ifc14xx-glibc-x86_64-ifc14xx-toolchain-ppc64e6500-toolchain-2.5-4.9.sh 
IFC14XX QorIQ SDK SDK installer version 2.5-4.9
===============================================
Enter target directory for SDK (default: /opt/ifc14xx/2.5-4.9): 
You are about to install the SDK to "/opt/ifc14xx/2.5-4.9". Proceed[Y/n]? 
SDK has been successfully set up and is ready to be used.
Each time you wish to use the SDK in a new shell session, you need to source the environment setup script e.g.
 $ . /opt/ifc14xx/2.5-4.9/environment-setup-ppc64e6500-fsl-linux
 $ . /opt/ifc14xx/2.5-4.9/environment-setup-ppce6500-fslmllib32-linux

[root@icslab-ser03 icssev]# ./ifc14xx-glibc-x86_64-ifc14xx-toolchain-ppc64e6500-toolchain-2.6-4.14.sh 
IFC14XX QorIQ SDK SDK installer version 2.6-4.14
================================================
Enter target directory for SDK (default: /opt/ifc14xx/2.6-4.14): 
You are about to install the SDK to "/opt/ifc14xx/2.6-4.14". Proceed[Y/n]? 

SDK has been successfully set up and is ready to be used.
Each time you wish to use the SDK in a new shell session, you need to source the environment setup script e.g.
 $ . /opt/ifc14xx/2.6-4.14/environment-setup-ppc64e6500-fsl-linux
 $ . /opt/ifc14xx/2.6-4.14/environment-setup-ppce6500-fslmllib32-linux

```


## Setup Curretn Compiler version

```
[root@icslab-ser03 icssev]# cd /opt/ifc14xx/
[root@icslab-ser03 ifc14xx]# ls
2.5-4.9  2.6-4.14
[root@icslab-ser03 ifc14xx]# ln -s 2.5-4.9 current
[root@icslab-ser03 ifc14xx]# ls
2.5-4.9  2.6-4.14  current
[root@icslab-ser03 ifc14xx]# ls -ltar
total 16
drwxr-xr-x. 7 root root 4096 Feb  1 15:57 ..
drwxr-xr-x. 3 root root 4096 Feb  1 15:57 2.5-4.9
drwxr-xr-x. 3 root root 4096 Feb  1 15:58 2.6-4.14
lrwxrwxrwx. 1 root root    7 Feb  1 15:59 current -> 2.5-4.9
drwxr-xr-x. 4 root root 4096 Feb  1 15:59 .

```

## Setup development tool

```
yum install centos-release-scl
yum-config-manager --enable rhel-server-rhscl-7-rpms
```

* gcc7

```
yum install devtoolset-7
```

* gcc 8
```
yum install devtoolset-8
```

## Enable gcc7 or gcc8

```
scl enable devtoolset-7 bash
```

```
scl enable devtoolset-8 bash
```
