#
#  Copyright (c) 2019 European Spallation Source ERIC
#
#  The program is free software: you can redistribute
#  it and/or modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation, either version 2 of the
#  License, or any newer version.
#
#  This program is distributed in the hope that it will be useful, but WITHOUT
#  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
#  more details.
#
#  You should have received a copy of the GNU General Public License along with
#  this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
#
# 
# Author  : Jeong Han Lee
# email   : jeonghan.lee@gmail.com
# Date    : Wednesday, February  6 14:50:09 CET 2019
# version : 0.0.1
#

where_am_I := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))

LEVEL?=1

# Makefile is located in Lodz_development/lib/neutron_detection/src
# clang++
# the final executable file is blm

RMOBJECTS:=

SW:=sw
BLM_DRV:=$(SW)/blm_driver
BLM_DRV_IC:=$(SW)/blm_driver_icblm
SW_LIB:=$(SW)/lib
SW_TSC:=$(SW)/userspace_tsc


LOTZ_LIB:=lib/neutron_detection
LOTZ_LIB_SRC:=$(LOTZ_LIB)/src
LOTZ_LIB_INC:=$(LOTZ_LIB_SRC)/include
LOTZ_LIB_INC_ETC:=$(LOTZ_LIB_INC)/etc


VPATH = $(BLM_DRV) ../tsc/lib ../tsc/include $(LOTZ_LIB_SRC)


INC_DIRS += $(LOTZ_LIB_SRC) $(LOTZ_LIB_INC) $(LOTZ_LIB_INC_ETC) ../tsc/include $(BLM_DRV)


CPPFLAGS += $(addprefix -I ,$(INC_DIRS))

CFLAGS   += -Wno-unused-variable
CFLAGS   += -Wno-unused-function
CFLAGS   += -Wno-unused-but-set-variable
CFLAGS   += -Wno-int-in-bool-context

CPPFLAGS += -Wno-unused-variable
CPPFLAGS += -Wno-unused-function
CPPFLAGS += -Wno-unused-but-set-variable
CPPFLAGS += -Wno-int-in-bool-context

CXXFLAGS   += -Wno-unused-variable
CXXFLAGS   += -Wno-unused-function
CXXFLAGS   += -Wno-unused-but-set-variable
CXXFLAGS   += -Wno-int-in-bool-context

vpath %.h $(INC_DIRS)


TSC_SOURCES = clilib.c  mtca4rtmlib.c  ponmboxlib.c  tscextlib.c  tsculib.c  tstlib.c
TSC_OBJECTS := $(addsuffix .o,$(basename $(TSC_SOURCES)))


RMOBJECTS+=$(TSC_OBJECTS)


CFLAGS   := -Wall  -O3 -DDATA_RECEIVER
CXXFLAGS := -Wall  -O3 -DDATA_RECEIVER -fpermissive
CXXFLAGS += -std=c++14
LDFLAGS  := -Wall -lpthread
LDFLAGS  += -lhdf5 -lhdf5_cpp


MKDIR    := mkdir -p
MV       := mv -f
RM       := rm -f
SED      := sed


install:
	@install -d -m 775 ../bin
	@install -m 775 $(BINSCXX) ../bin/
	@install -m 775 $(BINSC) ../bin/
	@$(RM) $(BINSC) $(BINSCXX)


show:
ifeq (, $(shell which tree))
	$(QUIET)ls -d $(where_am_I)
	$(QUIET)find $(where_am_I)/ -maxdepth $(LEVEL) | sed -e "s/[^-][^\/]*\// |/g" -e "s/|\([^ ]\)/:---\1/"
else
	$(QUIET)tree -L $(LEVEL) $(where_am_I)
endif


BINSC:= reg_gwj \
	mem_perf_tester


SRCSC = reg_gwj.c \
	ucsr.c   \
	resourceWin.c \
	mem_perf_tester.c



OBJSC = $(addsuffix .o,$(basename $(SRCSC)))
DEPSC  = $(subst .o,.d,$(OBJSC))


RMOBJECTS+=$(OBJSC)
RMOBJECTS+=$(DEPSC)


BINSCXX_SRC := dma_tester.cpp        \
	       cb_tester.cpp

BINSCXX     := $(BINSCXX_SRC:.cpp=)
BINSCXX     += mem_dma_reader_tsc
BINSCXX_OBJ :=$(BINSCXX_SRC:.cpp=.o)

RMOBJECTS+=$(BINSCXX_OBJ)

SRCSCXX = tsc_driver.cpp             \
	  algo_prepare_reference.cpp \
	  preprocess.cpp             \
	  neutron_summarizer.cpp     \
	  neutron_counter.cpp        \
	  neutron_aligner.cpp        \
	  detect.cpp                 \
	  data_source.cpp            \
	  mem_dma_reader.cpp         \
	  hdf5_interface.cpp

SRCSCXXC = crc32.c \
	   u256.c 

OBJSCXX = $(addsuffix .o,$(basename $(SRCSCXX)))
DEPSCXX = $(subst .o,.d,$(OBJSCXX))
RMOBJECTS+=$(OBJSCXX)
RMOBJECTS+=$(DEPSCXX)

OBJSCXXC = $(addsuffix .o,$(basename $(SRCSCXXC)))
DEPSCXXC = $(subst .o,.d,$(OBJSCXXC))
RMOBJECTS+=$(OBJSCXXC)
RMOBJECTS+=$(DEPSCXXC)


build:  $(BINSCXX) $(BINSC) 
	@echo $(BINSC) $(BINSCXX)

# Not yet implemented!
$(MODULE_BUILD_DIR):
	install -d -m 775 $(MODULE_BUILD_DIR)

# .c
reg_gwj: $(filter-out mem_perf_tester%,$(OBJSC))
	$(LINK.c) $^ $(LOADLIBES) $(LDLIBS) -o $@

mem_perf_tester: $(filter-out reg_gwj%,$(OBJSC))
	$(LINK.c) $^ $(LOADLIBES) $(LDLIBS) -o $@

dma_tester: dma_tester.o tsc_driver.o $(TSC_OBJECTS)
	$(LINK.cpp) $^ $(LOADLIBES) $(LDLIBS) -o $@

cb_tester: cb_tester.o $(OBJSCXX) $(OBJSCXXC) $(TSC_OBJECTS)
	$(LINK.cpp) $^ $(LOADLIBES) $(LDLIBS) -o $@

mem_dma_reader_tsc: tosca_main.o $(OBJSCXX) $(OBJSCXXC) $(TSC_OBJECTS)
	$(LINK.cpp) $^ $(LOADLIBES) $(LDLIBS) -o $@

RMOBJECTS+=tosca_main.o

# $(call make-depend,source-file,object-file,depend-file)
# define make-depend-c
#   $(CC) $(CFLAGS) $(CPPFLAGS) $(TARGET_ARCH) -M $1 | \
#   $(SED) 's,\($$(notdir $2)\) *:,$$(dir $2) $3: ,' > $3.tmp
#   $(MV) $3.tmp $3
# endef

%.o: %.c  | $(MODULE_BUILD_DIR)
	$(COMPILE.c) $(OUTPUT_OPTION) $<
#	$(call make-depend-c,$<,$@,$(subst .o,.d,$@))
#	$(COMPILE.c) -o $@ $<

# $(call make-depend,source-file,object-file,depend-file)
# define make-depend-cpp
#   $(CXX) $(CXXFLAGS) $(CPPFLAGS) $(TARGET_ARCH) -M $1 | \
#   $(SED) 's,\($$(notdir $2)\) *:,$$(dir $2) $3: ,' > $3.tmp
#   $(MV) $3.tmp $3
# endef

%.o: %.cpp  | $(MODULE_BUILD_DIR)
	$(COMPILE.cpp) $(OUTPUT_OPTION) $<
#	$(call make-depend-cpp,$<,$@,$(subst .o,.d,$@))
#	$(COMPILE.cpp) -o $@ $<



ifneq "$(MAKECMDGOALS)" "clean"
  -include $(DEPS)
endif


.PHONY: build builddir clean distclean show install $(MODULE_BUILD_DIR)


clean:
	@$(RM) -r $(RMOBJECTS) *.d $(MODULE_BUILD_DIR)


distclean: clean
	@$(RM) -r ../bin
