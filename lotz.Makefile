

LOTZ_LIB:=lib/neutron_detection
LOTZ_LIB_SRC:=$(LOTZ_LIB)/src
LOTZ_LIB_INC:=$(LOTZ_LIB)/include
LOTZ_LIB_INC_ETC:=$(LOTZ_LIB_INC)/etc


# Makefile is located in Lodz_development/lib/neutron_detection/src
# clang++
# the final executable file is blm


SW:=sw
BLM_DRV:=$(SW)/blm_driver
BLM_DRV_IC:=$(SW)/blm_driver_icblm
SW_LIB:=$(SW)/lib
SW_TSC:=$(SW)/userspace_tsc

# $(SW_TSC)/Makefile
# $(BLM_DRV)/{Makefile,Makefile.cpu,Makefile.local}
# $(BLM_DRV_IC)/Makefile.cpu

# First focus on $(BLM_DRV)/Makefile with cc toolchain.
#

# 
VPATH = $(LOTZ_LIB_INC) $(LOTZ_LIB_SRC)

CPPFLAGS += -I $(LOTZ_LIB_INC)
CPPFLAGS += -I $(LOTZ_LIB_SRC)
